#include <condition_variable>
#include <functional>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>

using namespace std;

template <typename T>
class MutexSafe
{
  public:
    MutexSafe( T* resource ) : _resource( resource ) {}
    ~MutexSafe()
    {
        delete _resource;
    }

    void lock()
    {
        _mutex.lock();
    }
    void unlock()
    {
        _mutex.unlock();
    }
    bool try_lock()
    {
        return _mutex.try_lock();
    }
    std::mutex& Mutex()
    {
        return _mutex;
    }
    T& Acquire( std::unique_lock<MutexSafe<T>>& lock )
    {
        MutexSafe<T>* _safe = lock.mutex();
        if ( &_safe->Mutex() != &_mutex )
        {
            throw "wrong lock object passed to Acquire function.\n";
        }
        return *_resource;
    }
    T& Acquire( std::unique_lock<std::mutex>& lock )
    {
        if ( lock.mutex() != &_mutex )
        {
            throw "wrong lock object passed to Acquire function.\n";
        }
        return *_resource;
    }

  private:
    std::mutex _mutex;
    T* _resource;
    T* operator->() {}
    T& operator&() {}
};

struct StockBlackboard
{
    float price;
    const std::string name;
    StockBlackboard( const string stockName, float stockPrice = 0 ) : name { stockName }, price { stockPrice } {}
};

typedef MutexSafe<StockBlackboard> StockSafe;

void PeterUpdateStock_Notify( StockSafe& safe, condition_variable& condition )
{
    for ( std::size_t i = 0; i < 4; ++i )
    {
        {
            std::unique_lock lock { safe };
            StockBlackboard& stock = safe.Acquire( lock );
            if ( i == 2 )
                stock.price = 99;
            else
                stock.price = std::abs( rand() % 100 );
            std::cout << "Peter udpated the price to $" << stock.price << std::endl;

            if ( stock.price > 90 )
            {
                lock.unlock();
                std::cout << "Peter notified Danny at price $" << stock.price << ", ";
                condition.notify_one();
                this_thread::sleep_for( 10ms );
                lock.lock();
            }
        }
        std::this_thread::sleep_for( std::chrono::milliseconds( rand() % 10 ) );
    }
}
void DannyWait_ReadStock( StockSafe& safe, condition_variable& priceCondition )
{
    std::unique_lock lock { safe.Mutex() };

    std::cout << "Danny is waiting for the right price to sell..." << std::endl;
    priceCondition.wait( lock );
    StockBlackboard& stock = safe.Acquire( lock );
    if ( stock.price > 90 )
        cout << "Danny sell at: $" << stock.price << endl;
    else
    {
        std::cout << "False alarm at $" << stock.price << " wait again..." << std::endl;
        priceCondition.wait( lock );
        StockBlackboard& stock = safe.Acquire( lock );
        if ( stock.price > 90 )
            cout << "Danny sell at: $" << stock.price << endl;
    }
}

void TestConditionVariable()
{
    StockSafe safe { new StockBlackboard( "APPL", 30 ) };
    std::condition_variable priceCondition;
    std::thread DannyThread( DannyWait_ReadStock, std::ref( safe ), std::ref( priceCondition ) );
    std::thread PeterThread( PeterUpdateStock_Notify, std::ref( safe ), std::ref( priceCondition ) );
    PeterThread.join();
    DannyThread.join();
}

int main()
{
    TestConditionVariable();
    return 0;
}
